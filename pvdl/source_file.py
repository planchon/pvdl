from os import path

class SourceFile:
    FLOW_RIGHT  = '>'
    FLOW_LEFT   = '<'
    FLOW_DOWN   = 'v'
    FLOW_UP     = '^'
    FLOW_PLUS   = '+'
    FLOW_MINUS  = '-'

    GOING_UP    = 1
    GOING_RIGHT = 2
    GOING_LEFT  = 3
    GOING_DOWN  = 4
    
    def __init__(self, file_path, brainfuck_mode=False):
        if not path.exists(file_path):
            print("\033[1mpbdq\033[0m - version 0.1")
            print("the file doesnt exists")
            exit(-1)

        self.file_ptr  = open(file_path, 'r')
        self.is_brainfuck = brainfuck_mode
        if not self.is_brainfuck:
            self.file_data = self.file_ptr.read().remove('\n')
            self.dim  = (len(self.file_data[0]), len(self.file_data))
        else:
            self.file_data = self.file_ptr.read().replace(' ', '').replace('\n', '')
            self.dim  = (len(self.file_data), 1)
        self.cursor    = [0, 0]

        self.flow = self.GOING_RIGHT 

    def default_follow_flow_function(self, char):
        return char not in [self.FLOW_DOWN, self.FLOW_LEFT, self.FLOW_MINUS, self.FLOW_PLUS, self.FLOW_RIGHT, self.FLOW_UP]

    def dumps_file(self):
        if not self.is_brainfuck:
            for i in range(self.dim[1]):
                for j in range(self.dim[0]):
                    if [i, j] != self.cursor:
                        print(self.file_data[i][j], end='', flush=True)
                    else:
                        print("\033[91m" + self.file_data[i][j] + "\033[0m", end='', flush=True)
                print()
            print()
        else:
            for i in range(self.dim[0]):
                if [0, i] != self.cursor:
                    print(self.file_data[i], end='', flush=True)
                else:
                    print("\033[91m" + self.file_data[i] + "\033[0m", end='', flush=True)
            print()

                
    # return the position of the next char to get interpreted
    def follow_flow(self, reverse=False, return_function=default_follow_flow_function):
        reverse_const = 0 if not reverse else -2
        current_char, _ = self.get_char(self.cursor)
        if return_function(self, current_char):
            # default TODO
            # right -> endl -> down -> begl
            if self.flow == self.GOING_RIGHT:
                if self.cursor[1] + 1 < self.dim[0]:
                    self.cursor[1] = self.cursor[1] + 1
                elif self.cursor[0] + 1 < self.dim[1]:
                    self.cursor[0] += 1
                    self.cursor[1] = 0
                else:
                    raise "out of source file"
            
            # left -> begl -> up -> endl
            if self.flow == self.GOING_LEFT:
                if self.cursor[1] - 1 >= 0:
                    self.cursor[1] = self.cursor[1] - 1
                elif self.cursor[0] - 1 >= 0:
                    self.cursor[0] -= 1
                    self.cursor[1] = self.dim[0] - 1
                else:
                    raise "out of source file"
            
            # up -> begf -> ednf col - 1
            if self.flow == self.GOING_UP:
                if self.cursor[0] - 1 >= 0:
                    self.cursor[0] = self.cursor[0] - 1
                elif self.cursor[1] - 1 >= 0:
                    self.cursor[0] -= 1
                    self.cursor[1] = self.dim[1] - 1
                else:
                    raise "out of source file"

            # down -> endf -> up col + 1
            if self.flow == self.GOING_DOWN:
                if self.cursor[0] + 1 < self.dim[1]:
                    self.cursor[0] = self.cursor[0] + 1
                elif self.cursor[0] + 1 < self.dim[0]:
                    self.cursor[1] = self.dim[1] - 1
                    self.cursor[0] += 1
                else:
                    raise "out of source file"

        else:
            if current_char == self.FLOW_DOWN:
                if (self.cursor[0] + 1) < self.dim[0]:
                    self.cursor[0] = self.cursor[0] + 1 + reverse_const
                    self.flow = self.GOING_DOWN
                else:
                    raise "out of source file (down op)"
            if current_char == self.FLOW_UP: 
                if (self.cursor[0] - 1) > 0:
                    self.cursor[0] = self.cursor[0] - 1 - reverse_const
                    self.flow = self.GOING_UP
                else:
                    raise "out of source file (up op)"
            if current_char == self.FLOW_RIGHT:
                if (self.cursor[1] + 1) < self.dim[1]:
                    self.cursor[1] = self.cursor[1] + 1 + reverse_const
                    self.flow = self.GOING_RIGHT
                else:
                    raise "out of source file (right op)"
            if current_char == self.FLOW_LEFT:
                if (self.cursor[1] - 1) > 0:
                    self.cursor[1] = self.cursor[1] - 1 - reverse_const
                    self.flow = self.GOING_LEFT
                else:
                    raise "out of source file (left op)"
            if current_char in [self.FLOW_PLUS, self.FLOW_MINUS]:
                raise "+ and - not correct for this version"

    def get_char(self, cursor):
        if not self.is_brainfuck:
            char, flow = self.file_data[cursor[0]][cursor[1]], self.flow    
        else:
            char, flow = self.file_data[cursor[1]], self.flow

        return char, flow

    def char_can_change_flow(self):
        char, _ = self.get_char(self.cursor)
        return char in [self.FLOW_DOWN, self.FLOW_LEFT, self.FLOW_MINUS, self.FLOW_PLUS, self.FLOW_RIGHT, self.FLOW_UP, ']', '[']

    def has_to_stop(self):
        if self.flow == self.GOING_RIGHT and self.cursor[1] + 1 >= self.dim[0] and not self.char_can_change_flow():
            return False
        if self.flow == self.GOING_LEFT and self.cursor[1] - 1 < 0 and not self.char_can_change_flow():
            return False
        if self.flow == self.GOING_UP and self.cursor[1] + 1 >= self.dim[1] and not self.char_can_change_flow():
            return False
        if self.flow == self.GOING_DOWN and self.cursor[1] - 1 < 0 and not self.char_can_change_flow():
            return False
        return True