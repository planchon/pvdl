import math

class Memory:
    def __init__(self):
        self.cursor = 0
        self.value = [0 for x in range(5)]

    # incrementation of the point
    def inc_cursor(self):
        if (self.cursor + 1) > len(self.value):
            self.value.append(0)
        self.cursor += 1

    def dec_cursor(self):
        if (self.cursor - 1) > len(self.value):
            raise "Cursor out of bounds"
        else:
            self.cursor -= 1

    def inc_value(self):
        self.value[self.cursor] += 1

    def dec_value(self):
        self.value[self.cursor] -= 1
    
    def dumps(self):
        taille_log = []
        for val in self.value:
            if val == 0:
                taille_log.append(1)
            elif val < 0:
                taille_log.append(int(math.log10(abs(val))) + 2)
            else:
                taille_log.append(int(math.log10(val)) + 1)            
        curs = [' ' * taille_log[i] if i != self.cursor else '\033[93mv\033[0m' + ' ' * (taille_log[i] - 1) for i in range(5)]
        stri = [str(self.value[i]) if i != self.cursor else '\033[93m' + str(self.value[i]) + '\033[0m' for i in range(5)]
        print('|' + ",".join([x for x in curs]) + '|')
        print('|' + ",".join([x for x in stri]) + '|')

    def get_value(self, pointer):
        return self.value[pointer]

    def set_value(self, value):
        self.value[self.cursor] = value

    def get_current_value(self):
        return self.value[self.cursor]