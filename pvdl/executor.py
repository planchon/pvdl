from source_file import SourceFile
from memory import Memory
import copy

class pvdl:
    PRINT       = '.'
    READ        = ','
    LOOP_BEGIN  = '['
    LOOP_END    = ']'
    INC_VALUE   = 'p'
    DEC_VALUE   = 'q'
    INC_POINTER = 'b'
    DEC_POINTER = 'd'

    MAX_PARSING = 1000

    def __init__(self, memory, source_file, ascii_mode=False):
        self.memory = memory
        self.source_file = source_file
        self.last_goto = []

        self.is_ascii_mode = ascii_mode

    def interprete(self, char, flow):
        if flow in [SourceFile.GOING_UP, SourceFile.GOING_LEFT]:
            self.LOOP_BEGIN, self.LOOP_END = ']', '['
        if flow in [SourceFile.GOING_DOWN, SourceFile.GOING_RIGHT]:
            self.LOOP_BEGIN, self.LOOP_END = '[', ']'
        
        if char == self.PRINT:
            if self.is_ascii_mode:
                print(chr(self.memory.get_value(self.memory.cursor)), end='', flush=True)
            else:
                print(self.memory.get_value(self.memory.cursor), end='', flush=True)
        if char == self.READ:
            # todo faire attention aux inputs
            self.memory.set_value(int(input()))

        if char == self.LOOP_BEGIN:    
            if self.source_file.cursor not in self.last_goto:
                self.last_goto.append(self.source_file.cursor.copy())
            else:
                pass

        if char == self.LOOP_END:
            if self.memory.get_current_value() != 0:
                self.source_file.cursor = self.last_goto[-1].copy()
            else:
                self.last_goto.pop()

        if char == self.INC_POINTER:
            self.memory.inc_cursor()
        if char == self.DEC_POINTER:
            self.memory.dec_cursor()
        if char == self.INC_VALUE:
            self.memory.inc_value()
        if char == self.DEC_VALUE:
            self.memory.dec_value()

    def run_once(self, cursor):
        char, flow = self.source_file.get_char(self.source_file.cursor)
        self.interprete(char, flow)
        # self.memory.dumps()
        # self.source_file.dumps_file()

    def parse(self):
        counter = 0
        while (self.source_file.has_to_stop()) and (counter < self.MAX_PARSING):
            if counter == 0:
                self.run_once([0,0])
            else:    
                self.run_once(self.source_file.cursor)

            counter += 1 
            self.source_file.follow_flow()
        
        self.run_once(self.source_file.cursor)
        